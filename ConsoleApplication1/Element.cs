﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ProteinCalculation
{
    public struct Element //параметры атома
    {
        public string nameElement;
        public double R;
        public int ep;
        public Element(string nameElement, double R , int ep)
        {
            this.nameElement = nameElement;
            this.R = R;
            this.ep = ep;
        }
        public Element(int R=0)
        {
            this.nameElement = null;
            this.R = 0;
            this.ep = 0;
        }
    }
}
