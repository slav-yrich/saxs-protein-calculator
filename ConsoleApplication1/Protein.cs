﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics; //Для комплексных чисел
using System.Windows; //Для двухмерных векторов
using System.Windows.Media.Media3D; //Для трёхмерных векторов

namespace ProteinCalculation
{
    public class Protein
    {
        public double x, y, z;//реальные координаты со сдвигом
        public double r;//Радиус в А
        public double xReal, yReal, zReal;//реальные координаты
        public int X, Y, Z, R; //приведённые размеры через размер кубика
        public string atom;
        public ElementGroup Info;
        public Protein(ref string line, ref DataBank dataBank)
        {
            //Console.WriteLine(line);
            //for (int i = 0; i < line.Count() ; i++ )
            //{
            //    Console.WriteLine("{0}: {1}", i+1, line[i]);
            //}
                ///
                ///Record Format
                ///
                ///COLUMNS        DATA  TYPE    FIELD        DEFINITION
                ///-------------------------------------------------------------------------------------
                ///1 -  6        Record name   "ATOM  "
                ///7 - 11        Integer       serial       Atom  serial number.
                ///13 - 16        Atom          name         Atom name.
                ///17             Character     altLoc       Alternate location indicator.
                ///18 - 20        Residue name  resName      Residue name.
                ///22             Character     chainID      Chain identifier.
                ///23 - 26        Integer       resSeq       Residue sequence number.
                ///27             AChar         iCode        Code for insertion of residues.
                ///31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
                ///39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
                ///47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
                ///55 - 60        Real(6.2)     occupancy    Occupancy.
                ///61 - 66        Real(6.2)     tempFactor   Temperature  factor.
                ///77 - 78        LString(2)    element      Element symbol, right-justified.
                ///79 - 80        LString(2)    charge       Charge  on the atom.
                ///



            string[] subLine = 
            {
                line.Substring(0,6).Trim(),  ///0 | 1 - 6     "ATOM  "
                line.Substring(6,5).Trim(),  ///1 | 7 - 11    serial       Atom  serial number.
                line.Substring(12,4).Trim(), ///2 | 13 - 16   name         Atom name.
                line.Substring(16,1).Trim(), ///3 | 17        altLoc       Alternate location indicator.
                line.Substring(17,3).Trim(), ///4 | 18 - 20   resName      Residue name.
                line.Substring(21,1).Trim(), ///5 | 22        Character     chainID      Chain identifier.
                line.Substring(22,4).Trim(), ///6 | 23 - 26   Integer       resSeq       Residue sequence number.
                line.Substring(26,1).Trim(), ///7 | 27        AChar         iCode        Code for insertion of residues.
                line.Substring(30,8).Trim(), ///8 | 31 - 38   Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
                line.Substring(38,8).Trim(), ///9 | 39 - 46   Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
                line.Substring(46,8).Trim(), ///10| 47 - 54   Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
                line.Substring(54,6).Trim(), ///11| 55 - 60   Real(6.2)     occupancy    Occupancy.
                line.Substring(60,6).Trim(), ///12| 61 - 66   Real(6.2)     tempFactor   Temperature  factor.
                "",//line.Substring(76,2).Trim(), ///13| 77 - 78   LString(2)    element      Element symbol, right-justified.
                ""//line.Substring(78,2).Trim()  ///14| 79 - 80   LString(2)    charge       Charge  on the atom.
            };
            //foreach (string s in subLine)
            //{
            //    Console.WriteLine(s);
            //}

            xReal = x = Convert.ToDouble(subLine[8]);
            yReal = y = Convert.ToDouble(subLine[9]);
            zReal = z = Convert.ToDouble(subLine[10]);

            //Костыль, если нет столбца с названием атомов!!!
            //atom = subLine[13];
            {
                short buffer;
                atom = subLine[2].Substring(0,1);
                if (Int16.TryParse(atom, out buffer))
                {
                    atom = subLine[2].Substring(1, 1);
                }
            }

            ////для файлов после обработки (удаления/добавления водородов
            //atom = subLine[2];
            //atom = ((atom[0] >= '0') && (atom[0] <= '9')) ? atom[1].ToString() : atom[0].ToString();

            ElementGroup EG = new ElementGroup(0);
            if (dataBank.hydrogenCoordinates)
            {
                
                EG.mainAtom = dataBank.GetInfoaboutAtom(atom);
                EG.R = EG.mainAtom.R;
                EG.ep = EG.mainAtom.ep;
            }
            else
            {
                EG = dataBank.TranslateElementGroup(subLine);
            }

            ////Только на время теста!!!
            //if (EG.aminoName == null) dataBank.ElementGroupConst.Add(new ElementGroup(subLine[3],subLine[4],subLine[2],dataBank.GetInfoaboutAtom(atom),0,0));


            Info = EG;
            r = Info.R;
        }

        public void rating(ref DataBank dataBank)//Генерация относительных координат центров атомов и координат выраженных в ячейках массива
        {
            x = x + dataBank.shiftVector.X;
            y = y + dataBank.shiftVector.Y;
            z = z + dataBank.shiftVector.Z;

            X = Convert.ToInt32(x / dataBank.cellsD);
            Y = Convert.ToInt32(y / dataBank.cellsD);
            Z = Convert.ToInt32(z / dataBank.cellsD);
            R = Convert.ToInt32(r / dataBank.cellsD);
        }


        public void AtomAccessSphere(ref DataBank dataBank)//построение доступного объёма атомов белка с добавкой
        {
            Sphere(x, y, z, r + dataBank.rSolvent, dataBank.cellsD, ref dataBank.aquarium, 1);

        }

        public static void HydroSphere(int x, int y, int z, ref DataBank dataBank)//построение сфер атомов растворителя
        {
            Sphere(x * dataBank.cellsD + dataBank.cellsD / 2, y * dataBank.cellsD + dataBank.cellsD / 2, z * dataBank.cellsD + dataBank.cellsD / 2, dataBank.rSolvent, dataBank.cellsD, ref dataBank.aquarium, 0);
        }

        public static void Sphere(double sphX0, double sphY0, double sphZ0, double sphR, double cellsD, ref Cell[, ,] aquarium, byte operation)
        {
            int
                ixBeg = Convert.ToInt32((sphX0 - sphR) / cellsD) - 1,
                iyBeg = Convert.ToInt32((sphY0 - sphR) / cellsD) - 1,
                izBeg = Convert.ToInt32((sphZ0 - sphR) / cellsD) - 1,
                ix,
                iy,
                iz,
                ixEnd = Convert.ToInt32((sphX0 + sphR) / cellsD) + 1,
                iyEnd = Convert.ToInt32((sphY0 + sphR) / cellsD) + 1,
                izEnd = Convert.ToInt32((sphZ0 + sphR) / cellsD) + 1;

            double a = cellsD / 2;
            double RCell = Math.Sqrt(2) * 0.5 * cellsD,//описанной
                   rCell = 0.5 * cellsD,//вписанной
                   AB,
                   x, y, z,
                   xBeg = ixBeg * cellsD + cellsD / 2,
                   yBeg = iyBeg * cellsD + cellsD / 2,
                   zBeg = izBeg * cellsD + cellsD / 2;
            bool flag;
            double xdVerif, ydVerif, zdVerif;
            for (ix = ixBeg, x = xBeg; ix <= ixEnd; ix++, x += cellsD)
            {
                for (iy = iyBeg, y = yBeg; iy <= iyEnd; iy++, y += cellsD)
                {
                    for (iz = izBeg, z = zBeg; iz <= izEnd; iz++, z += cellsD)
                    {
                        AB = Math.Sqrt((x - sphX0) * (x - sphX0) + (y - sphY0) * (y - sphY0) + (z - sphZ0) * (z - sphZ0));

                        switch (operation)
                        {
                            case 1://заполнение доступного объёма белка
                                {
                                    if ((AB + 0.003) < sphR)
                                    {
                                        aquarium[ix, iy, iz].value = 1;
                                        aquarium[ix, iy, iz].AccessVolume = true;


                                    }

                                    //if ((Math.Abs(AB - sphR) <= RCell)&&(Math.Abs(AB + sphR) >= RCell))
                                    //{
                                    //    flag = false;
                                    //    for (int i = 0; ((i <= 2)&&(!flag)); i++)
                                    //    {
                                    //        for (int j=0;((j<=2)&&(!flag));j++)
                                    //        {
                                    //            for (int k = 0; ((k <= 2) && (!flag)); k++)
                                    //            {
                                    //                //a=CellsD/2
                                    //                xdVerif = x + a * ((i == 0) ? 0 : 1) * Math.Pow(-1, i);
                                    //                ydVerif = y + a * ((j == 0) ? 0 : 1) * Math.Pow(-1, j);
                                    //                zdVerif = z + a * ((k == 0) ? 0 : 1) * Math.Pow(-1, k);
                                    //                flag = (Math.Sqrt((xdVerif - sphX0) * (xdVerif - sphX0) + (ydVerif - sphY0) * (ydVerif - sphY0) + (zdVerif - sphZ0) * (zdVerif - sphZ0))<=sphR);
                                    //            }
                                    //        }
                                    //    }
                                    //    if (flag)
                                    //    {
                                    //        aquarium[ix, iy, iz].AccessSurface = true;
                                    //    }
                                    //}
                                    //if ((sphR-AB>0)&&(sphR-AB<=rCell))
                                    //{
                                    //    aquarium[ix, iy, iz].value = 2;
                                    //}

                                    //else
                                    //    if //(((AB - RCell) <= sphR) || 
                                    //        ((AB - rCell) <= sphR)
                                    //    //Для учёта тех кубиков, которые хотя бы од
                                    //    {
                                    //        //все ли граничные точки являются внутренними??? - нет!
                                    //        if (aquarium[ix, iy, iz].value != 1) aquarium[ix, iy, iz].value = 2;
                                    //    }
                                    break;
                                }
                            case 0://вычитание объёма растворителя из доступного объёма
                                {
                                    if ((AB - 0.003) < sphR) //ИСПРАВЛЯЙ ПЛОЩАДЬ АТОМОВ РАСТВОРИТЕЛЯ
                                    {
                                        aquarium[ix, iy, iz].value = 0;
                                        aquarium[ix, iy, iz].SolventArea = true;
                                    }
                                    //if ((AB + RCell) < sphR)
                                    //{
                                    //    aquarium[ix, iy, iz].value = 0;
                                    //}
                                    //else
                                    //    if (((AB - RCell) <= sphR)
                                    //        || ((AB - rCell) <= sphR))
                                    //    {
                                    //        //if (aquarium[ix, iy, iz].value == 2) aquarium[ix, iy, iz].value = 0;
                                    //        if (aquarium[ix, iy, iz].MolecularSurface) aquarium[ix, iy, iz].value = 0;
                                    //        if (aquarium[ix, iy, iz].value != 0) aquarium[ix, iy, iz].value = 3;
                                    //    }
                                    break;
                                }
                        }


                    }
                }
            }
        }

        public static void CalculationMolecularSurface(ref DataBank dataBank)//Заполнение объёмов и расчёт поверхностей
        {
            dataBank.numberAccessSurfaceCells = 0;
            dataBank.NumberAccessVolumeCell = 0;
            dataBank.numberMolecularSurfaceCells = 0;
            dataBank.NumberMolecularVolumeCell = 0;
            dataBank.SumElectrons = 0;

            foreach (Protein a in dataBank.atoms)
            {
                a.AtomAccessSphere(ref dataBank); //Заполняем доступный объём в молекулярном объёме для редактирования
                dataBank.SumElectrons += a.Info.ep;
            }

            //for (int x = 0; x < dataBank.aSizeX; x++)
            //    for (int y = 0; y < dataBank.aSizeY; y++)
            //        for (int z = 0; z < dataBank.aSizeZ; z++)
            //        {
            //            if (dataBank.aquarium[x, y, z].value == 1) dataBank.NumberAccessVolumeCell++;
            //            else if (dataBank.aquarium[x, y, z].value == 2) dataBank.numberAccessSurfaceCells++;
            //        }

            ///////ищем границы
            for (int x = 0; x < dataBank.aSizeX; x++)
                for (int y = 0; y < dataBank.aSizeY; y++)
                    for (int z = 0; z < dataBank.aSizeZ; z++)
                    {
                        //if (dataBank.aquarium[x, y, z].value == 1) dataBank.NumberAccessVolumeCell++;
                        if (dataBank.aquarium[x, y, z].AccessVolume) dataBank.NumberAccessVolumeCell++;
                        if ((dataBank.aquarium[x, y, z].value == 2)
                            || ((dataBank.aquarium[x, y, z].value == 1)
                            && ((dataBank.aquarium[x - 1, y, z].value == 0) || (dataBank.aquarium[x + 1, y, z].value == 0)
                            || (dataBank.aquarium[x, y - 1, z].value == 0) || (dataBank.aquarium[x, y + 1, z].value == 0)
                            || (dataBank.aquarium[x, y, z - 1].value == 0) || (dataBank.aquarium[x, y, z + 1].value == 0))
                            ))
                        {
                            dataBank.numberAccessSurfaceCells++;
                            dataBank.aquarium[x, y, z].AccessSurface = true;
                        }
                        else
                        {
                            dataBank.aquarium[x, y, z].AccessSurface = false;
                        }

                    }

            //for (int x = 0; x < dataBank.aSizeX; x++)
            //    for (int y = 0; y < dataBank.aSizeY; y++)
            //        for (int z = 0; z < dataBank.aSizeZ; z++)
            //        {
            //            if (dataBank.aquarium[x, y, z].AccessVolume) dataBank.NumberAccessVolumeCell++;
            //            if (dataBank.aquarium[x, y, z].AccessSurface) dataBank.numberAccessSurfaceCells++;
            //        }

            dataBank.accessArea = dataBank.cellAreaBig * dataBank.numberAccessSurfaceCells;
            dataBank.accessVolume = dataBank.NumberAccessVolumeCell * dataBank.cellsD * dataBank.cellsD * dataBank.cellsD;

            //вычитаем растворитель из доступного объёма (в молекулярном)
            for (int x = 0; x < dataBank.aSizeX; x++)
                for (int y = 0; y < dataBank.aSizeY; y++)
                    for (int z = 0; z < dataBank.aSizeZ; z++)
                        if (dataBank.aquarium[x, y, z].AccessSurface)
                        {
                            Protein.HydroSphere(x, y, z, ref dataBank);
                        }

            ////Убираем дырки
            for (int x = 0; x < dataBank.aSizeX; x++)
                for (int y = 0; y < dataBank.aSizeY; y++)
                    for (int z = 0; z < dataBank.aSizeZ; z++)
                    {
                        if (dataBank.aquarium[x, y, z].value == 1)
                        {
                            bool hole;

                            if (dataBank.aquarium[x + 1, y, z].value == 0)
                            {
                                //по X
                                hole = false;
                                for (int x_ = x + dataBank.m; x_ >= x; x_--)
                                {
                                    if (!hole)
                                    {
                                        if ((dataBank.aquarium[x_, y, z].value == 1) && (x_ != x))
                                        {
                                            hole = true;
                                            dataBank.aquarium[x_, y, z].value = 1;
                                        }
                                    }
                                    else
                                    {
                                        dataBank.aquarium[x_, y, z].value = 1;
                                    }

                                }
                            }

                            if (dataBank.aquarium[x, y + 1, z].value == 0)
                            {
                                //по Y
                                hole = false;
                                for (int y_ = y + dataBank.m; y_ >= y; y_--)
                                {
                                    if (!hole)
                                    {
                                        if ((dataBank.aquarium[x, y_, z].value == 1) && (y_ != y))
                                        {
                                            hole = true;
                                            dataBank.aquarium[x, y_, z].value = 1;
                                        }
                                    }
                                    else
                                    {
                                        dataBank.aquarium[x, y_, z].value = 1;
                                    }

                                }
                            }

                            if (dataBank.aquarium[x, y, z + 1].value == 0)
                            {
                                //по Z
                                hole = false;
                                for (int z_ = z + dataBank.m; z_ >= z; z_--)
                                {
                                    if (!hole)
                                    {
                                        if ((dataBank.aquarium[x, y, z_].value == 1) && (z_ != z))
                                        {
                                            hole = true;
                                            dataBank.aquarium[x, y, z_].value = 1;
                                        }
                                    }
                                    else
                                    {
                                        dataBank.aquarium[x, y, z_].value = 1;
                                    }

                                }
                            }
                        }
                    }

            //Восстанавливаем границу молекулярного пространства
            for (int x = 1; x < dataBank.aSizeX - 1; x++)
                for (int y = 1; y < dataBank.aSizeY - 1; y++)
                    for (int z = 1; z < dataBank.aSizeZ - 1; z++)
                    {
                        //if ((dataBank.aquarium[x, y, z].value == 1)
                        //    && ((dataBank.aquarium[x - 1, y, z].value == 0) || (dataBank.aquarium[x + 1, y, z].value == 0)
                        //    || (dataBank.aquarium[x, y - 1, z].value == 0) || (dataBank.aquarium[x, y + 1, z].value == 0)
                        //    || (dataBank.aquarium[x, y, z - 1].value == 0) || (dataBank.aquarium[x, y, z + 1].value == 0)))
                        //{
                        //    dataBank.aquarium[x, y, z].MolecularSurface = true;
                        //    dataBank.numberMolecularSurfaceCells++;
                        //}
                        //else
                        //{
                        //    dataBank.aquarium[x, y, z].MolecularSurface = false;
                        //}

                        if ((dataBank.aquarium[x, y, z].SolventArea)
                            && ((dataBank.aquarium[x - 1, y, z].value == 1) || (dataBank.aquarium[x + 1, y, z].value == 1)
                            || (dataBank.aquarium[x, y - 1, z].value == 1) || (dataBank.aquarium[x, y + 1, z].value == 1)
                            || (dataBank.aquarium[x, y, z - 1].value == 1) || (dataBank.aquarium[x, y, z + 1].value == 1)))
                        {
                            dataBank.aquarium[x, y, z].MolecularSurface = true;
                            dataBank.numberMolecularSurfaceCells++;
                            dataBank.aquarium[x, y, z].MolecularVolume = true;
                            dataBank.NumberMolecularVolumeCell++;
                        }

                        if (dataBank.aquarium[x, y, z].value == 1)
                        {
                            dataBank.aquarium[x, y, z].MolecularVolume = true;
                            dataBank.NumberMolecularVolumeCell++;
                        }

                        //if (dataBank.aquarium[x, y, z].value == 3) dataBank.aquarium[x, y, z].surface = true;
                    }
            dataBank.molecularArea = dataBank.cellAreaSmall * dataBank.numberMolecularSurfaceCells;
            dataBank.molecularVolume = dataBank.NumberMolecularVolumeCell * dataBank.cellsD * dataBank.cellsD * dataBank.cellsD;
        }
        public static void CalculationVolume(ref DataBank dataBank)//Подсчёт объёмов молекулярного и доступного пространств
        {
            dataBank.molecularVolume = 0;
            dataBank.accessVolume = 0;
            double cellVolume = dataBank.cellsD * dataBank.cellsD * dataBank.cellsD;
            for (int x = 0; x < dataBank.aSizeX; x++)
                for (int y = 0; y < dataBank.aSizeY; y++)
                    for (int z = 0; z < dataBank.aSizeZ; z++)
                    {
                        if (dataBank.aquarium[x, y, z].value > 0)
                            dataBank.molecularVolume += cellVolume;
                        //Console.WriteLine(dataBank.molecularVolume);

                    }
        }
        public static void CalculationArea(ref DataBank dataBank)
        {

            int molecularAreaNumber = 0,
                accessAreaNumber = 0;
            double cellAreaSmall = 0.097,//коэффициент для сфер радиуса от 1,5А до 2А
                   cellAreaBig = 0.101;//коэффициент для сфер радиуса от 3А до 3,5А
            for (int x = 0; x < dataBank.aSizeX; x++)
                for (int y = 0; y < dataBank.aSizeY; y++)
                    for (int z = 0; z < dataBank.aSizeZ; z++)
                    {
                        if (dataBank.aquarium[x, y, z].MolecularSurface)
                            molecularAreaNumber++;
                        if (dataBank.aquarium[x, y, z].AccessSurface)
                            accessAreaNumber++;
                    }

            dataBank.accessArea = cellAreaBig * accessAreaNumber; //Так сфера доступной поверхности это около ~1,7+~1,5=~3.2, то берём второй коэффициент
            dataBank.molecularArea = cellAreaSmall * molecularAreaNumber;

        }

        //public static void NumberCalculation(ref DataBank dataBank)//Метод исключительно для тестирования
        //{
        //    int molec = 0,
        //        molecSurf = 0,
        //        access = 0,
        //        accessSurf = 0;

        //    double cellVolume = dataBank.cellsD * dataBank.cellsD * dataBank.cellsD;
        //    for (int x = 0; x < dataBank.aSizeX; x++)
        //        for (int y = 0; y < dataBank.aSizeY; y++)
        //            for (int z = 0; z < dataBank.aSizeZ; z++)
        //            {
        //                if (dataBank.aquarium[x, y, z].molecular)
        //                    molec++;
        //                if (dataBank.aquarium[x, y, z].access)
        //                    access++;
        //                if (dataBank.aquarium[x, y, z].molecularSurface)
        //                    molecSurf++;
        //                if (dataBank.aquarium[x, y, z].accessSurface)
        //                    accessSurf++;

        //            }
        //    Console.WriteLine("\tКоличество ячеек в:\n\t\taccess={0}\n\t\taccessSurface={1}\n\t\tmolecular={2}\n\t\tmolecularSurface={3}",access,accessSurf,molec,molecSurf);
        //}


        public static void SummaryElctrons(ref DataBank dataBank)
        {
            dataBank.SumElectrons = 0;
            foreach (Protein prot in dataBank.atoms)
            {
                dataBank.SumElectrons += prot.Info.ep;
            }
        }

        public static void GetChainCubes(ref DataBank dataBank)
        {
            int numberCoubs, firstCoubZ = 0, counter = 0;
            dataBank.listChainCubs = new List<ChainCubes>();
            for (int ix = 0; ix < dataBank.aSizeX; ix++)
            {
                for (int iy = 0; iy < dataBank.aSizeY; iy++)
                {
                    numberCoubs = 0;
                    firstCoubZ = 0;
                    for (int iz = 0; iz < dataBank.aSizeZ; iz++)
                    {
                        if (dataBank.aquarium[ix, iy, iz].MolecularVolume)
                        {
                            if (numberCoubs == 0)
                            {
                                firstCoubZ = iz;
                            }
                            numberCoubs++;
                        }
                        else
                        {
                            if (numberCoubs > 0)
                            {
                                dataBank.listChainCubs.Add(new ChainCubes(ix, iy, firstCoubZ, numberCoubs));
                                numberCoubs = 0;

                                counter++;
                            }
                        }
                    }
                }
            }

            //Console.WriteLine(counter);
        }

        public static double CalcRadiusInertia(ref DataBank databank)
        {
            double R = 0;
            double numerator = 0, denumerator = 0;

            foreach (Protein atom1 in databank.atoms)
            {
                foreach (Protein atom2 in databank.atoms)
                {
                    numerator += atom1.Info.ep * atom2.Info.ep
                                * ((atom2.xReal - atom1.xReal) * (atom2.xReal - atom1.xReal) + (atom2.yReal - atom1.yReal) * (atom2.yReal - atom1.yReal) + (atom2.zReal - atom1.zReal) * (atom2.zReal - atom1.zReal));
                    denumerator += atom1.Info.ep * atom2.Info.ep;
                }
            }
            if (denumerator != 0)
                R = Math.Sqrt(numerator / 2 / denumerator);
            else R = -1;

            return R;
        }

    }
}