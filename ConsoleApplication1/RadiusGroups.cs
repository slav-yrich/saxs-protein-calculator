﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProteinCalculation
{
    public struct RadiusGroups
    {
        public string atomName;
        public int hydrogenNumber;
        public double R;

        public RadiusGroups(string atomName, int HydrogenNumber, double R)
        {
            this.atomName = atomName;
            this.hydrogenNumber = HydrogenNumber;
            this.R = R;
        }
    }
}
