﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml.Linq;
//using Excel = Microsoft.Office.Interop.Excel;
//using Office = Microsoft.Office.Core;
//using Microsoft.Office.Tools.Excel;

namespace ProteinCalculation
{
    public class Reporter
    {
        
        public static void ReportIntensityTxt(string reportDirectory, string proteinName, ref DataBank dataBank, int currentModel)
        {
            Directory.CreateDirectory(reportDirectory);
      
            StreamWriter IntensityReportFile = new StreamWriter(string.Format("{0}\\Intens[{1}_model_{2}_po={3:00.00}]_q=[{4:00.00};{5:00.00}]={6}_{7}x{8}.txt",
                                                               reportDirectory, proteinName, currentModel, dataBank.ro0, dataBank.qBeg, dataBank.qEnd, dataBank.numberQ, dataBank.numberQz, dataBank.numberFi));
            
            foreach (IntensityofQ iofq in dataBank.IofQ)
            {
                IntensityReportFile.WriteLine("{0}\t{1}",iofq.Q,Math.Log10(iofq.I));
            }

            IntensityReportFile.Close();
        }

        //public static void ReportIntensityExel(string reportDirectory, string proteinName, ref DataBank dataBank)
        //{
        //    //Создаем эксельку.
        //    Microsoft.Office.Interop.Excel.Application ObjExcel = new Microsoft.Office.Interop.Excel.Application();
        //    Microsoft.Office.Interop.Excel.Workbook ObjWorkBook;
        //    Microsoft.Office.Interop.Excel.Worksheet ObjWorkSheet;
        //    //Книга.
        //    ObjWorkBook = ObjExcel.Workbooks.Add(System.Reflection.Missing.Value);
        //    //Таблица.
        //    ObjWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)ObjWorkBook.Sheets[1];
        //    //Заполняем значениями.
        //    //Значения [y - строка,x - столбец]
        //    ObjWorkSheet.Cells[01,01] = "Q";
        //    ObjWorkSheet.Cells[01, 02] = "lg(I(q))";
        //    int i = 02;
        //    foreach (IntensityofQ iofq in dataBank.IofQ)
        //    {
        //        ObjWorkSheet.Cells[i, 01] = iofq.Q.ToString();
        //        ObjWorkSheet.Cells[i, 2] =  Math.Log10(iofq.I).ToString();
        //        i++;
        //    }
        //    //В итоге, делаем созданную эксельку видимой и доступной!
        //    ObjExcel.Visible = true;
        //    ObjExcel.UserControl = true;
        //}

        //public void WriteReport(string fileProteinName,ref DataBank dataBank)
        //{

        //string reportDirectoryName = "Report_[" + fileProteinName + "]_[" + DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute +"_"+ DateTime.Now.Second + "]";
        //Directory.CreateDirectory(reportDirectoryName);
        //string accessDirectoryName = reportDirectoryName + @"\Access\Layers\" + "Solvent_" + dataBank.rSolvent + "_Kub_" + dataBank.cellsD+"_["+dataBank.aSizeX+"x"+dataBank.aSizeY+"x"+dataBank.aSizeZ+"]",
        //       molecDirectoryName = reportDirectoryName + @"\Molecular\Layers\" + "Solvent_" + dataBank.rSolvent + "_Kub_" + dataBank.cellsD+"_["+dataBank.aSizeX+"x"+dataBank.aSizeY+"x"+dataBank.aSizeZ+"]";
        //Directory.CreateDirectory(accessDirectoryName);
        //Directory.CreateDirectory(molecDirectoryName);


        //    string accessSurfaceLayerFileName,
        //           molecularSurfaceLayerFileName;
        //    StreamWriter accessSurfaceLayerFile,
        //                molecularSurfaceLayerFile;
        //    for (int x = 0; x < dataBank.aSizeX; x++)
        //    {
        //        accessSurfaceLayerFileName = accessDirectoryName + @"\" + "[" + fileProteinName + "]" + "_Sloy_" + x.ToString() + "_Solvent_" + dataBank.rSolvent + "_Kub_" + dataBank.cellsD + "[" + dataBank.aSizeX + "x" + dataBank.aSizeY + "x" + dataBank.aSizeZ + "]" + ".prn";
        //        accessSurfaceLayerFile = new StreamWriter(accessSurfaceLayerFileName);

        //        molecularSurfaceLayerFileName = molecDirectoryName + @"\" + "[" + fileProteinName + "]" + "_Sloy_" + x.ToString() + "_Solvent_" + dataBank.rSolvent + "_Kub_" + dataBank.cellsD + "[" + dataBank.aSizeX + "x" + dataBank.aSizeY + "x" + dataBank.aSizeZ + "]" + ".prn";
        //        molecularSurfaceLayerFile = new StreamWriter(molecularSurfaceLayerFileName);

        //        for (int y = 0; y < dataBank.aSizeY; y++)
        //        {
        //            for (int z = 0; z < dataBank.aSizeZ; z++)
        //            {
        //                if (dataBank.aquarium[x, y, z].accessSurface)
        //                {
        //                    accessSurfaceLayerFile.WriteLine(x.ToString() + " " + y.ToString() + " " + z.ToString() + "\n");
        //                }
        //                if (dataBank.aquarium[x, y, z].molecularSurface)
        //                {
        //                    molecularSurfaceLayerFile.WriteLine(x.ToString() + " " + y.ToString() + " " + z.ToString() + "\n");
        //                }

        //            }
        //        }
        //        accessSurfaceLayerFile.Close();
        //        molecularSurfaceLayerFile.Close();
        //    }
        //}
    }
}
