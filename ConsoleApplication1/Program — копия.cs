﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace ProteinCalculation
{
    class Program
    {
        static void Main()
        {
            Console.Write("Добро пожаловать. Введите адрес файла, содержащего константы для расчёта и адрес файла-белка:\n\t");
            string ConstFileName = "dataFile.txt";// Console.ReadLine();
            try
            {
                DataBank dataBank = new DataBank(ConstFileName);
                if (dataBank != null)
                {
                    Console.WriteLine("Список белков для расчёта и константы успешно считаны.\n\tСписок файлов для расчётов:");
                    foreach (string fprotname in dataBank.filesProteinName)
                    {
                        Console.WriteLine("\t\t" + fprotname);
                    }
                    Console.WriteLine("\tВаан-дер-Ваальсовый радиус растворителя = " + dataBank.rSolvent.ToString() + "A"
                                     + "\n\tРазмер ячейки = " + dataBank.cellsD.ToString() + "A");
                    Console.WriteLine("Начинаю обработку файлов. Время начала расчётов: " + (DateTime.Now).ToString());
                    foreach (string fprotname in dataBank.filesProteinName)
                    {
                        Console.WriteLine("\tНачинаю обработку файла \"" + fprotname + "\" \n\t\tВремя начала: " + (DateTime.Now).ToString());
                        dataBank.ProteinRead(fprotname, ref dataBank);
                        Console.WriteLine("\t\tДанные успешно считаны"
                                           + "\n\t\tРазмеры массива разбиения: " + dataBank.aSizeX.ToString() + "x"
                                                                                 + dataBank.aSizeY.ToString() + "x"
                                                                                 + dataBank.aSizeZ.ToString());
                        Protein.CalculationMolecularSurface(ref dataBank);
                        Console.WriteLine("\t\tПостроение молекулярного и доступного пространств завершено.");
                        Protein.CalculationVolume(ref dataBank);
                        Console.WriteLine("\t\tДоступный объём = " + (dataBank.accessVolume).ToString()+" A^3"
                                          + "\n\t\tМолекулярный объём = " + (dataBank.molecularVolume).ToString()+" A^3");
                        Console.WriteLine("p = {0}",7962/dataBank.molecularVolume);
                        //Protein.CalculationArea(ref dataBank);
                        //Console.WriteLine("\t\tПлощадь доступной = " + (dataBank.accessArea).ToString()+" A^2"
                        //                  + "\n\t\tПлощадь молекулярной поверхности = " + (dataBank.molecularArea).ToString()+" A^2");
                        //Здесь добавлять вызов новых расчётов
                        //dataBank.WriteReport(fprotname, ref dataBank);
                        //Console.WriteLine("\t\tПапка с отчётом создана.");
                        //Protein.NumberCalculation(ref dataBank);
                        Protein.CalculationScatteringIntensity(ref dataBank);
                        Console.WriteLine("\t\tВремя завершения: " + (DateTime.Now).ToString() + "\n\tОбработка файла \"" + fprotname + "\" завершена");
                    }
                    Console.WriteLine("Обработка файлов завершена. Время завершения расчётов: " + (DateTime.Now).ToString()
                                      + "\n\nДля выхода подвердите ввод.");
                }

                

            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }


            Console.ReadKey();
        }
        }
        
}
