﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProteinCalculation
{
    public struct ElementGroup
    {
        public string aminoName; //наименование аминокислоты к кторой принадлежит группа
        public string isometry; //изомер аминокислоты
        public string pdbGroupName; //наименование группы в файле pdb
        public Element mainAtom; //группообразующий атом (О,N,C...)
        public int hydrogenNumber; //число водородных атомов
        public int ep;// сумарное число электронов
        public double R; //радиус группы

        public ElementGroup(int R=0)
        {
            aminoName=null; 
            isometry=null; 
            pdbGroupName=null; 
            mainAtom=new Element(0); 
            hydrogenNumber=0; 
            ep=0;
            this.R=0;
        }

        public ElementGroup(string aminoName, string isometry, string pdbGroupName, Element mainAtom, int hydrogenNumber, double R=0)
        {
            this.aminoName = aminoName;
            this.isometry = isometry;
            this.pdbGroupName = pdbGroupName;
            this.mainAtom = mainAtom;
            this.hydrogenNumber = hydrogenNumber;
            this.ep = mainAtom.ep+hydrogenNumber;
            if (R!=0)
            {
                this.R = R;
            }
            else
            {
                this.R = mainAtom.R;
            }
        }

    }
}
