﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;


namespace ProteinCalculation
{
    
    public partial class PrintStructWindows : Form
    {
        public bool flag;
        public DataBank dataBank;
        public int z;
        public PrintStructWindows(ref DataBank dataBank)
        {
            InitializeComponent();
            this.Width = this.Height;
            this.dataBank = dataBank;
            z = 0;
            toolStripLabel1.Text = "Слой (0<=Z<="+(dataBank.aSizeZ-1).ToString()+"):";
            toolStripTextBox1.Text = "0";
            flag = true;
        }

        private void PrintStructWindows_Load(object sender, EventArgs e)
        {
        }

        private void PrintStructWindows_ResizeEnd(object sender, EventArgs e)
        {
            flag = true;
            this.Refresh();
        }

        public void Render(Graphics graphics)
        {
            //int z = 27;
            //int cellsD = (ClientSize.Width / dataBank.aSizeX >= ClientSize.Height / dataBank.aSizeY) ? (ClientSize.Height / dataBank.aSizeY) : (ClientSize.Width / dataBank.aSizeX);
            int cellsD = (this.Width / dataBank.aSizeX >= this.Height / dataBank.aSizeY) ? (this.Height / dataBank.aSizeY) : (this.Width / dataBank.aSizeX);
            HatchBrush brush;
            Rectangle rect;

            for (int x = 0; x < dataBank.aSizeX; x++)
            {
                for (int y = 0; y < dataBank.aSizeY; y++)
                {
                    rect = new Rectangle(x * cellsD, y * cellsD, cellsD, cellsD);
                    brush = new HatchBrush(HatchStyle.Percent20, Color.Aqua, Color.White);
                    if (dataBank.aquarium[x, y, z].MolecularVolume)
                    {
                        brush = new HatchBrush(HatchStyle.Percent20, Color.Green, Color.White);
                        graphics.FillRectangle(brush, rect);
                        if (dataBank.aquarium[x, y, z].MolecularSurface)
                        {
                            //brush = new HatchBrush(HatchStyle.BackwardDiagonal, Color.Black, Color.White);
                            graphics.FillRectangle(Brushes.Green, rect);
                            graphics.DrawRectangle(Pens.Silver, rect);
                        }
                    }
                    else
                        if (dataBank.aquarium[x, y, z].AccessVolume)
                        {
                            brush = new HatchBrush(HatchStyle.Percent20, Color.LightBlue, Color.White);
                            graphics.FillRectangle(brush, rect);
                            if (dataBank.aquarium[x, y, z].AccessSurface)
                            {
                                //brush = new HatchBrush(HatchStyle.BackwardDiagonal, Color.Blue, Color.White);
                                graphics.FillRectangle(Brushes.LightBlue, rect);
                                graphics.DrawRectangle(Pens.Silver, rect);
                            }
                        }





                    if (toolStripButton5.Checked) graphics.DrawRectangle(Pens.Silver, rect);
                }
            }

            double k = dataBank.cellsD / cellsD,   //коэффициент перевода (ангстремы в пиксели)
                   zd, currentR, shiftZd;

            if ((toolStripButton2.Checked)||(toolStripButton3.Checked))
            {
                int irSolvent = Convert.ToInt32(dataBank.rSolvent / dataBank.cellsD);
                zd = z * dataBank.cellsD + dataBank.cellsD / 2; //высота текущего слоя в ангстрем
                
                foreach (Protein atom in dataBank.atoms)
                {

                    if (toolStripButton2.Checked)
                    {
                        if ((z >= atom.Z - atom.R) && (z <= atom.Z + atom.R))
                        {

                            shiftZd = Math.Abs(atom.z - zd); //сдвиг между срезом теущего атома и его центром
                            currentR = Math.Sqrt(Math.Abs(atom.r * atom.r - shiftZd * shiftZd));
                            graphics.DrawEllipse(Pens.Red, (float)((atom.x - currentR) / k), (float)((atom.y - currentR) / k),
                                                (float)(2 * currentR / k), (float)(2 * currentR / k));

                        }
                    }

                    if (toolStripButton3.Checked) //строим доступный радиус
                    {
                        if ((z >= atom.Z - (atom.R + irSolvent)) && (z <= atom.Z + (atom.R + irSolvent)))
                        {
                            
                            shiftZd = Math.Abs(atom.z - zd); //сдвиг между срезом теущего атома и его центром
                            currentR = Math.Sqrt(Math.Abs((atom.r + dataBank.rSolvent) * (atom.r + dataBank.rSolvent) - shiftZd * shiftZd));
                            graphics.DrawEllipse(Pens.Black, (float)((atom.x - currentR) / k), (float)((atom.y - currentR) / k),
                                                (float)(2 * currentR / k), (float)(2 * currentR / k));

                        }
                    }
                }
            }

            if (toolStripButton4.Checked)
            {
                int rSolventpixels = Convert.ToInt32(dataBank.rSolvent / k); //диаметр растворителя в пикселях
                for (int x = 0; x < dataBank.aSizeX; x++)
                {
                    for (int y = 0; y < dataBank.aSizeY; y++)
                    {
                        if (dataBank.aquarium[x,y,z].AccessSurface)
                        {
                            graphics.DrawEllipse(Pens.Blue, x * cellsD + cellsD - rSolventpixels, y * cellsD + cellsD - rSolventpixels,
                                                2 * rSolventpixels, 2 * rSolventpixels);
                        }
                    }
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //using (Bitmap bitmap = new Bitmap(ClientSize.Width, ClientSize.Height))
            //using (Graphics graphics = Graphics.FromImage(bitmap))
            //{
            //    Render(graphics);
            //    Clipboard.SetImage(bitmap);
            //}
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (flag)
            {
                Render(e.Graphics);
                
            }
        }

        private void PrintStructWindows_ResizeBegin(object sender, EventArgs e)
        {
            flag = false;
        }

        public void drowThis()
        {
            bool okflag = false;
            if (toolStripTextBox1.Text != "")
            {
                okflag = true;
                for (int i = 0; ((i < toolStripTextBox1.Text.Count()) && (okflag)); i++)
                {
                    okflag = ((toolStripTextBox1.Text[i] >= '0') && (toolStripTextBox1.Text[i] <= '9'));
                }
                if (okflag)
                {
                    int z_ = Convert.ToInt32(toolStripTextBox1.Text);
                    if ((z_ >= 0) && (z_ < dataBank.aSizeZ))
                    {
                        this.z = z_;

                        this.Refresh();
                    }
                    else okflag = false;
                }
            }
            if (!okflag) MessageBox.Show("Ошибка в номере слоя! Повторите ввод.");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            drowThis();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void toolStripTextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                drowThis();
            }
        }

        



    }
}
