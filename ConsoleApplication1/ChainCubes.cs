﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProteinCalculation
{
    public class ChainCubes
    {
        public int ix, iy, iz; //номер первого кубика цепочки
        public int numberCoubs; //число кубиков

        public ChainCubes(int ix, int iy, int iz, int numberCoubs)
        {
            this.ix = ix;
            this.iy = iy;
            this.iz = iz;
            this.numberCoubs = numberCoubs;
        }
    }
}
