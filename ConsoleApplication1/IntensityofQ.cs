﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics; //Для комплексных чисел
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ProteinCalculation
{
    public class IntensityofQ
    {
        /// <summary>
        /// Основной класс для рассчёта интенсивности рентгеновского малоуглового рассеяния (МУРР) от белка.
        /// Экземляр класса содержит радиус сферы обратного пространство, и модуль рассеяния от этой сферы.
        /// Статичный метод CalculationScatteringIntensity вызывается из основного класса программы для расчёта всей индикатрисы МУРР.
        /// </summary>

        public double I;
        public double Q;

        public IntensityofQ(double Q)
        {
            this.I = 0;
            this.Q = Q;
        }

        public IntensityofQ(double I, double Q)
        {
            this.I = I;
            this.Q = Q;
        }

        public void DebayCalculationScatteringIntensityofQ(ref DataBank dataBank)//Рассчёт интенсивность МУРР для сферы обратного пространства текущего радиуса по формуле Дебая
        {
            double q = this.Q;
            double Rij = 0;
            double I = 0;
           

            for (int i=0; i<dataBank.atoms.Count;i++)
            {
                for (int j=0;j<dataBank.atoms.Count;j++)
                {
                    if (i != j)
                    {
                        Rij = Math.Sqrt((dataBank.atoms[i].xReal - dataBank.atoms[j].xReal) * (dataBank.atoms[i].xReal - dataBank.atoms[j].xReal)
                                        + (dataBank.atoms[i].yReal - dataBank.atoms[j].yReal) * (dataBank.atoms[i].yReal - dataBank.atoms[j].yReal)
                                        + (dataBank.atoms[i].zReal - dataBank.atoms[j].zReal) * (dataBank.atoms[i].zReal - dataBank.atoms[j].zReal));
                        I +=  (dataBank.atoms[i].Info.ep * dataBank.atoms[j].Info.ep * Math.Sin(Rij * q) / (Rij * q));
                    }
                    else
                    {
                        I +=  dataBank.atoms[i].Info.ep * dataBank.atoms[j].Info.ep;
                    }

                }
            }

            this.I = I;
            Console.Write("*");
        }
        public void CalculationScatteringIntensityofQ(ref DataBank dataBank)//Рассчёт интенсивность МУРР для сферы обратного пространства текущего радиуса по формуле Фёдорова
        {
            double fiStep = 2 * Math.PI / dataBank.numberFi,//dfi
                   fi,
                   q,
                   qz, qx, qy, //вектор обратного пространства
                   sinaX, sinaY, sinaZ,
                   sqrtQQz,
                   qzStep,
                   qStep = (dataBank.qEnd - dataBank.qBeg) / dataBank.numberQ,
                   scalarQRj,
                   a = dataBank.cellsD / 2,
                   I;

            int numberI;

            Complex Aatoms, //Амплитуда рассеяния от белка
                    Asolvent, //Амлитуда рассеяния от расстворителя, занимающего объём белка
                    A; //Амплитуда рассеяния "пустот"

            q=this.Q;
            {
                I = 0;
                numberI = 0;

                qzStep = 2 * q / dataBank.numberQz;
                for (qz = -q; qz <= q; qz += qzStep)
                //qz = 0;
                {
                    sqrtQQz = Math.Sqrt(q * q - qz * qz);
                    if (qz != 0)
                    {
                        sinaZ = Math.Sin(qz * a) / qz;
                    }
                    else
                    {
                        sinaZ = a;
                    }
                    //for (fi = 0; fi <= 2 * Math.PI; fi += fiStep)
                    for (fi = 0; fi < 2 * Math.PI; fi += fiStep)
                    {
                        Aatoms = new Complex();
                        Asolvent = new Complex();


                        qx = sqrtQQz * Math.Cos(fi);
                        if (qx != 0)
                        {
                            sinaX = Math.Sin(qx * a) / qx;
                        }
                        else
                        {
                            sinaX = a;
                        }

                        qy = sqrtQQz * Math.Sin(fi);
                        if (qy != 0)
                        {
                            sinaY = Math.Sin(qy * a) / qy;
                        }
                        else
                        {
                            sinaY = a;
                        }

                        foreach (Protein jAtom in dataBank.atoms)
                        {
                            scalarQRj = qx * jAtom.xReal + qy * jAtom.yReal + qz * jAtom.zReal;
                            Aatoms += jAtom.Info.ep * (new Complex(Math.Cos(scalarQRj), Math.Sin(scalarQRj)));
                        }

                        //Расчёт амлитуды рассеяния от раствора производится только если у нас есть плотность растворителя
                        if (dataBank.ro0 != 0)
                        {
                            double xjd = 0, yjd = 0, zjd = 0;
                            for (int i = 0; i < dataBank.listChainCubs.Count; i++)
                            {
                                xjd = dataBank.listChainCubs[i].ix * dataBank.cellsD + a - dataBank.shiftVector.X;
                                yjd = dataBank.listChainCubs[i].iy * dataBank.cellsD + a - dataBank.shiftVector.Y;
                                zjd = (dataBank.listChainCubs[i].iz + 0.5 * dataBank.listChainCubs[i].numberCoubs) * dataBank.cellsD - dataBank.shiftVector.Z;

                                if (qz != 0)
                                {
                                    sinaZ = Math.Sin(qz * a * dataBank.listChainCubs[i].numberCoubs) / qz;
                                }
                                else
                                {
                                    sinaZ = a * dataBank.listChainCubs[i].numberCoubs;
                                }
                                scalarQRj = qx * xjd + qy * yjd + qz * zjd;
                                Asolvent += sinaZ * (new Complex(Math.Cos(scalarQRj), Math.Sin(scalarQRj)));
                            }
                            //int xj = 0, yj = 0, zj = 0;
                            //for (xj = 0; xj < dataBank.aSizeX; xj++)
                            //{
                            //    xjd = xj * dataBank.cellsD + a - dataBank.shiftVector.X;
                            //    for (yj = 0; yj < dataBank.aSizeY; yj++)
                            //    {
                            //        yjd = yj * dataBank.cellsD + a - dataBank.shiftVector.Y;
                            //        for (zj = 0; zj < dataBank.aSizeZ; zj++)
                            //        {
                            //            zjd = zj * dataBank.cellsD + a - dataBank.shiftVector.Z;
                            //            if (dataBank.aquarium[xj, yj, zj].MolecularVolume)
                            //            {
                            //                scalarQRj = qx * xjd + qy * yjd + qz * zjd;
                            //                Asolvent += new Complex(Math.Cos(scalarQRj), Math.Sin(scalarQRj));
                            //            }
                            //        }
                            //    }
                            //}
                        }

                        A = Aatoms - dataBank.ro0 * 8 * sinaX * sinaY * /*sinaZ*/  Asolvent;
                        I += (A * Complex.Conjugate(A)).Real;

                        numberI++;
                    }


                }

                I /= numberI;
                //Console.WriteLine("Lg(I({0}))={1}", q, Math.Log10(I));
                //Console.WriteLine("I({0})={1}", q, I);
                this.I = I;

                Console.Write(".");
                //timer.Stop();
                //dataBank.timeOfPoint = (dataBank.timeOfPoint + timer.Elapsed.TotalMinutes) / 2;
                //Console.Write("\r\t\t\t\t\t\t ");//сие есть костыль. ПРИДУМАЙ как очищать строку
                //Console.Write("\r{0}/{1}|осталось {2:0.####} мин.", ++dataBank.progressIndicator, dataBank.numberQ,(dataBank.numberQ-dataBank.progressIndicator)/dataBank.ProcessorCount*dataBank.timeOfPoint);
            }
        }

        //public static void CalculationScatteringIntensity(ref DataBank dataBank)
        //{
        //    dataBank.IofQ = new List<IntensityofQ>();
        //    double qStep = (dataBank.qEnd - dataBank.qBeg) / dataBank.numberQ;
        //    for (double q = dataBank.qBeg; q < dataBank.qEnd; q += qStep)
        //    {
        //        dataBank.IofQ.Add(new IntensityofQ(q));
        //    }
        //    Stopwatch timer = new Stopwatch();
        //    timer.Start();
        //    foreach (IntensityofQ iofq in dataBank.IofQ)
        //    {
        //        iofq.CalculationScatteringIntensityofQ(ref dataBank);
        //    }
        //    timer.Stop();
        //    Console.WriteLine("\t\t\tTime \"for\"= {0}", timer.Elapsed);
        //}

        public static List<IntensityofQ> CalculationScatteringIntensity(DataBank dataBank)
        {
            //Console.WriteLine(System.Environment.ProcessorCount); //Количество процессоров
            dataBank.progressIndicator = 0;
            Console.WriteLine();
            dataBank.IofQ = new List<IntensityofQ>();
            double qStep = (dataBank.qEnd - dataBank.qBeg) / dataBank.numberQ;


            for (double q = dataBank.qBeg; q <= dataBank.qEnd; q += qStep)
            {
                dataBank.IofQ.Add(new IntensityofQ(q));
            }

            Stopwatch timer = new Stopwatch();
            timer.Start();

            //foreach(IntensityofQ iofq in dataBank.IofQ)
            Parallel.ForEach(dataBank.IofQ, iofq =>
                {
                    iofq.CalculationScatteringIntensityofQ(ref dataBank);
                    //Формула Дебая: оставлена на всякий случай, если потребуется проверка основной формулы
                    //iofq.DebayCalculationScatteringIntensityofQ(ref dataBank);
                }
                );
            Console.WriteLine();
            dataBank.IofQ = (
             from iofq in dataBank.IofQ
             orderby iofq.Q
             select iofq).ToList();
            //Console.WriteLine("\n"+dataBank.IofQ.Count);
            //foreach (IntensityofQ iofq in dataBank.IofQ)
            //{
            //    Console.WriteLine("I({0})={1}", iofq.Q, Math.Log10(iofq.I));
            //}
            timer.Stop();
            Console.WriteLine("\t\t\tTime \"Parallel)forEach\"= {0}", timer.Elapsed);
            return dataBank.IofQ;
        }

    }
}
