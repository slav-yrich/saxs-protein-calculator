﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Media.Media3D; //Для трёхмерных векторов

namespace ProteinCalculation
{
    public class DataBank
    {
        public List<Protein> atoms; //атомы белка
        public List<String> filesProteinName; //список адресов файлов белков для расчётов 
        public List<Element> ElementConst; //лист параметров атомов, которые могут встретится в белке
        public List<RadiusGroups> RadiusGroupsConst; //лист параметров групп (атом+водородные атомы), а именно их радиусы (при необходимости доп описаний, можно добавить)
        public List<ElementGroup> ElementGroupConst; //лист описаний аминокислот для перевода  названий файла pdb во внутреннюю систему обозачений
        public List<IntensityofQ> IofQ;//лист значений интенсивности и q
        public List<ChainCubes> listChainCubs;
        public Cell[, ,] aquarium; //трёхмерная система
        public int countModel; //количество моделее описанных в файле
        public int aSizeX, aSizeY, aSizeZ;
        public double rSolvent, //Радиус Ван-дер-Ваальса растворителя (воды)
                      cellsD; //Размер ячейки системы (сторона или пол стороны, нужно решить!)

        public bool hydrogenCoordinates; //наличие описания координат атомов водорода

        public double minX, minY, minZ;//реальная координата нулевой ячейки массива
        public double molecularVolume, accessVolume;
        public double molecularArea, accessArea;

        public int numberFi, numberQz, numberQ;//количество шагов при разбиении сферы обратного пространства
        public double qBeg, qEnd;
        public double ro0;
        public Vector3D shiftVector;
        public int m;//параметр для ликвидации "дырок"
        public int SumElectrons;
        public int numberAccessSurfaceCells = 0, //количество кубиков
                NumberAccessVolumeCell = 0,
                numberMolecularSurfaceCells = 0,
                NumberMolecularVolumeCell = 0;
        //аналог progressBar
        public int progressIndicator;
        public byte ProcessorCount;
        public double timeOfPoint;

        public double cellAreaSmall = 0.081,//коэффициент для сфер радиуса от 1,5А до 2А
                   cellAreaBig = 0.094;//коэффициент для сфер радиуса от 3А до 3,5А

        //public double cellAreaSmall = 0.105,//коэффициент для сфер радиуса от 1,5А до 2А
        //           cellAreaBig = 0.099;//коэффициент для сфер радиуса от 3А до 3,5А


        public DataBank(string fileConstName) //Считываем список файлов для расчёта и константы
        {
            StreamReader ConstFile = null;
            //сначала читаем файл, в котором содержатся константы и имя файла описывающего белок
            try
            {
                ConstFile = new StreamReader(fileConstName);
                try
                {
                    ///
                    ///нужно поработать над форматирование, сделать его более удобочитаемым, с описание
                    ///можно добавить список фалов вместо одного файла, но тогда стоит вопрос в том, как производить расчёт.
                    ///нужно уточнить, нужно ли это вообще.
                    ///
                    int numberProteinFiles = Convert.ToInt32(ConstFile.ReadLine());
                    filesProteinName = new List<string>();
                    for (; numberProteinFiles > 0; numberProteinFiles--)
                    {
                        filesProteinName.Add(ConstFile.ReadLine().Trim());
                    }

                    hydrogenCoordinates = (ConstFile.ReadLine().Trim()[0] == '+'); //+ - есть описания атомов водородов
                    m = Convert.ToInt32(ConstFile.ReadLine());

                    cellsD = Convert.ToDouble(ConstFile.ReadLine());

                    
                    //m = Convert.ToInt32(rSolvent / cellsD);
                    //m = 4;

                    

                    //Console.WriteLine(cellsD);
                    rSolvent = Convert.ToDouble(ConstFile.ReadLine());

                    //numberFi = 28;
                    numberFi = Convert.ToInt32(ConstFile.ReadLine()); 
                    //numberQz = 28;
                    numberQz = Convert.ToInt32(ConstFile.ReadLine());
                    //numberQ = 20;// 100;
                    numberQ = Convert.ToInt32(ConstFile.ReadLine());
                    //qBeg = 0.05;
                    qBeg = Convert.ToDouble(ConstFile.ReadLine());
                    //qEnd = 1.6;
                    qEnd = Convert.ToDouble(ConstFile.ReadLine());
                    //ro0 = 0.334;
                    //ro0 = 0.411;
                    ro0 = Convert.ToDouble(ConstFile.ReadLine());


                    //создаём лист параметров атомов
                    ElementConst = new List<Element>();
                    int numberElement = Convert.ToInt32(ConstFile.ReadLine());
                    for (; numberElement > 0; numberElement--)
                    {
                        ElementConst.Add(new Element(ConstFile.ReadLine().Trim(),
                                                        Convert.ToDouble(ConstFile.ReadLine()),
                                                        Convert.ToInt32(ConstFile.ReadLine())));
                    }





                    

                    //Console.WriteLine(m);
                    //ProteinRead(fileProteinName);

                    ProcessorCount = (byte)System.Environment.ProcessorCount;



                    //Считываем файл описаний аминокислот для перевода  названий файла pdb во внутреннюю систему обозачений
                    ElementGroupConst = new List<ElementGroup>();
                    string DescriptionAminoAcidsFileName = "DescriptionAminoAcids.txt";
                    StreamReader DescriptionAminoAcidsFile = new StreamReader(DescriptionAminoAcidsFileName);
                    string line;
                    //DescriptionAminoAcidsFile.ReadLine(); //Пропускаем строку описания формата файла
                    while ((line = DescriptionAminoAcidsFile.ReadLine()) != null)
                    {
                        char[] separator = new char[1];
                        separator[0] = ' ';
                        string[] subLine = line.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                        ElementGroup EG = new ElementGroup(subLine[0], subLine[1], subLine[2], GetInfoaboutAtom(subLine[3]), Convert.ToInt32(subLine[4]), Convert.ToDouble(subLine[5]));
                        ElementGroupConst.Add(EG);
                        //Console.WriteLine("->{0} {1} {2} {3} {4} {5}", subLine[0], subLine[1], subLine[2], subLine[3], subLine[4], subLine[5]);
                        //Console.WriteLine("{0} {1} {2} {3} {4} {5}", EG.aminoName, EG.isometry, EG.pdbGroupName, EG.mainAtom.nameElement, EG.hydrogenNumber, EG.R);
                    }

                    DescriptionAminoAcidsFile.Close();

                    //Считываем файл с радиусами групп
                    RadiusGroupsConst = new List<RadiusGroups>();

                }
                catch (Exception e)
                {
                    if (e == null)
                        throw new Exception("Не удалось считать данные из файла с константами. Возможно файл неверно отформатирован или повреждён.");
                    else throw e;
                }
            }
            catch (Exception e)
            {
                if (e == null)
                    throw new Exception("Файл с константами не найден или его не удалось открыть.");
                else throw e;
            }
            finally
            {
                if (ConstFile != null) ConstFile.Close();
            }

        }

        public void ProteinRead(string fileProteinName, ref DataBank dataBank,int currentModel) //Метод считывает данные из файла о белке, создаёт листинг атомов белка и трёхмерное пространство по этим данным
        {
            atoms = new List<Protein>();
            //
            double
                maxX = -1000, maxY = -1000, maxZ = -1000,
                minX_ = 1000, minY_ = 1000, minZ_ = 1000,
                maxX_ = -1000, maxY_ = -1000, maxZ_ = -1000,
                RminX = 0, RminY = 0, RminZ = 0, //Радиусы крайних атомов
                RmaxX = 10, RmaxY = 10, RmaxZ = 10;
            string line;

            minX = 1000; minY = 1000; minZ = 1000;

            StreamReader file = null;
            //int counterDeleteHOH=0;
            bool flag=(currentModel==0);
            try
            {
                file = new StreamReader(fileProteinName);
                try
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        if ((line.Length >= 14) && (line.Substring(0, 5) == "MODEL") && (Convert.ToInt32(line.Substring(10, 4))==currentModel))
                        {
                            flag = true;
                        }
                        if (flag)
                        {
                            if ((line.Length >= 6) && ((line.Substring(0, 4) == "ATOM") || (line.Substring(0, 6) == "HETATM")))
                            {
                                atoms.Add(new Protein(ref line, ref dataBank));
                                //Определяем граничные атомы и их радиусы

                                if (atoms[atoms.Count - 1].x - atoms[atoms.Count - 1].r < minX_) { minX = atoms[atoms.Count - 1].x; RminX = atoms[atoms.Count - 1].r; minX_ = minX - RminX; }
                                if (atoms[atoms.Count - 1].y - atoms[atoms.Count - 1].r < minY_) { minY = atoms[atoms.Count - 1].y; RminY = atoms[atoms.Count - 1].r; minY_ = minY - RminY; }
                                if (atoms[atoms.Count - 1].z - atoms[atoms.Count - 1].r < minZ_) { minZ = atoms[atoms.Count - 1].z; RminZ = atoms[atoms.Count - 1].r; minZ_ = minZ - RminZ; }
                                if (atoms[atoms.Count - 1].x + atoms[atoms.Count - 1].r > maxX_) { maxX = atoms[atoms.Count - 1].x; RmaxX = atoms[atoms.Count - 1].r; maxX_ = maxX + RmaxX; }
                                if (atoms[atoms.Count - 1].y + atoms[atoms.Count - 1].r > maxY_) { maxY = atoms[atoms.Count - 1].y; RmaxY = atoms[atoms.Count - 1].r; maxY_ = maxY + RmaxY; }
                                if (atoms[atoms.Count - 1].z + atoms[atoms.Count - 1].r > maxZ_) { maxZ = atoms[atoms.Count - 1].z; RmaxZ = atoms[atoms.Count - 1].r; maxZ_ = maxZ + RmaxZ; }
                            }

                            if ((line.Length >= 6) && (line.Substring(0, 6) == "ENDMDL"))
                            {
                                break;
                            }
                            //if ((line.Length >= 7) && ((line.Substring(0, 5) == "*ATOM") || (line.Substring(0, 7) == "*HETATM")))
                            //{
                            //    counterDeleteHOH++;
                            //}
                        }
                    }
                    


                    ////Создание файла описаний аминокислот
                    //string DescriptionAminoAcidsFileName = "DescriptionAminoAcids.txt";
                    //StreamWriter DescriptionAminoAcidsFile = new StreamWriter(DescriptionAminoAcidsFileName);
                    //foreach (ElementGroup eg in dataBank.ElementGroupConst)
                    //{
                    //    ElementGroup EG = eg;
                    //    if (EG.pdbGroupName == "N")
                    //    { EG.hydrogenNumber = 2; }
                    //    else
                    //        if (EG.pdbGroupName == "CA")
                    //        { EG.hydrogenNumber = 1; }
                    //        else
                    //            if (EG.pdbGroupName == "C")
                    //            { EG.hydrogenNumber = 0; }
                    //            else
                    //                if (EG.pdbGroupName == "O")
                    //                { EG.hydrogenNumber = 0; }
                    //    DescriptionAminoAcidsFile.WriteLine("{0} {1} {2} {3} {4} {5}", EG.aminoName, EG.isometry, EG.pdbGroupName, EG.mainAtom.nameElement, EG.hydrogenNumber, EG.R);
                    //}
                    //DescriptionAminoAcidsFile.Close();

                    ////Создание файла описаний групп
                    //string DescriptionGroupFileName = "DescriptionGroup.txt";
                    //StreamWriter DescriptionGroupFile = new StreamWriter(DescriptionGroupFileName);
                    //foreach (ElementGroup EG in dataBank.ElementGroupConst)
                    //{
                    //    RadiusGroups RG = new RadiusGroups(EG.mainAtom.nameElement, EG.hydrogenNumber, EG.R);
                    //    bool flag=false;
                    //    foreach (RadiusGroups rg in dataBank.RadiusGroupsConst)
                    //    {
                    //        flag=(RG.atomName==rg.atomName)&&(RG.hydrogenNumber==rg.hydrogenNumber);
                    //        if (flag) break;
                    //    }
                    //    if (!flag)
                    //    {
                    //        dataBank.RadiusGroupsConst.Add(RG);
                    //        DescriptionGroupFile.WriteLine("{0} H {1} {2}", RG.atomName, RG.hydrogenNumber, RG.R);
                    //    }
                    //}
                    //DescriptionGroupFile.Close();

                    //Console.ReadKey();

                    //Делаем поправку на радиусы крайних атомов и радиус водорода + 3 клетки для дальнейшего упрощения расчётов
                    minX -= RminX + 3 * rSolvent + 4 * cellsD;
                    minY -= RminY + 3 * rSolvent + 4 * cellsD;
                    minZ -= RminZ + 3 * rSolvent + 4 * cellsD;
                    maxX += RmaxX + 3 * rSolvent + 4 * cellsD;
                    maxY += RmaxY + 3 * rSolvent + 4 * cellsD;
                    maxZ += RmaxZ + 3 * rSolvent + 4 * cellsD;

                    //Находим вектор сдвига
                    shiftVector = new Vector3D(Math.Abs(minX) * (-1) * Math.Sign(minX),
                                                        Math.Abs(minY) * (-1) * Math.Sign(minY),
                                                        Math.Abs(minZ) * (-1) * Math.Sign(minZ));
                    dataBank.shiftVector = shiftVector;

                    //Рассчитываем размеры массива
                    aSizeX = Convert.ToInt32((maxX + shiftVector.X) / cellsD);
                    aSizeY = Convert.ToInt32((maxY + shiftVector.Y) / cellsD);
                    aSizeZ = Convert.ToInt32((maxZ + shiftVector.Z) / cellsD);
                    try
                    {
                        //создаём трехмерное пространство по рассчитаным размерам
                        aquarium = new Cell[aSizeX, aSizeY, aSizeZ];

                        //Генерируем относительные координаты атомов белка в виртуальном пространстве
                        foreach (Protein a in atoms)
                        {
                            a.rating(ref dataBank);
                        }
                    }
                    catch (Exception e)
                    {
                        if (e.Message == null)
                            throw new Exception(e.Message + "Не удалось выделить память под трёхмерное пространство. Возможно заданы слишком высокая точность или большое количество атомов.");
                        else throw e;
                    }
                }
                catch (Exception e)
                {
                    if (e.Message == null)
                        throw new Exception(e.Message + "Не удалось считать данные из файла с описанием белка. Возможно файл неверно отформатирован или повреждён.");
                    else throw e;
                }
            }
            catch (Exception e)
            {
                if (e.Message == null)
                    throw new Exception("Файл с описанием белка не найден или его не удалось открыть.");
                else throw e;
            }
            finally
            {
                if (file != null) file.Close();
            }
        }

        public Element GetInfoaboutAtom(string nameElement)
        {
            foreach (Element atom in ElementConst)
            {
                if (atom.nameElement == nameElement) return atom;
            }

            //throw new Exception("Обнаружен атом [" + nameElement + " ], описание которого отсутствует. Повторите расчёт после добавления.");
            return new Element("defaultAtom",1.74,6);//Нужно реализовать редактирование файлов в случае отсутствия

        }

        public ElementGroup TranslateElementGroup(string[] subLine)
        {
            foreach (ElementGroup EG in ElementGroupConst)
            {
                //Влияет ли измотерия на результат?
                if ((EG.aminoName == subLine[4])) //&& (EG.isometry == subLine[5]))
                {
                    if (EG.pdbGroupName == subLine[2]) return EG;
                }
            }
            //throw new Exception("Обнаруженa запись №:"+subLine[1]+" [" + subLine[4] + " " + subLine[5] + " " + subLine[2] + " ], описание которой отсутствует. Повторите расчёт после добавления описаний в файл DescriptionAminoAcids.txt");
            return new ElementGroup("DefaultAmino", "A", "DefaultGroup", (new Element("defaultAtom",1.74,6)),1,2);//Нужно реализовать редактирование файлов в случае отсутствия
        }

        public void ModelCounter(string fileProteinName)
        {
            this.countModel = 0;
            string line;
            StreamReader file = null;
            try
            {
                file = new StreamReader(fileProteinName);
                try
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        if ((line.Length >= 6) && (line.Substring(0, 5) == "MODEL"))
                        {
                            this.countModel++;
                        }
                    }
                }
                catch (Exception e)
                {
                    if (e.Message == null)
                        throw new Exception(e.Message + "Не удалось считать данные из файла с описанием белка. Возможно файл неверно отформатирован или повреждён.");
                    else throw e;
                }
            }
            catch (Exception e)
            {
                if (e.Message == null)
                    throw new Exception("Файл с описанием белка не найден или его не удалось открыть.");
                else throw e;
            }
            finally
            {
                if (file != null) file.Close();
            }
        }

    }
}
