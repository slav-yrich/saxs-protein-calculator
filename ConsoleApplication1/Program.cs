﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

using System.Globalization;
using System.Diagnostics;

namespace ProteinCalculation
{
    public class Program
    {
        static void Main()
        {
            //Языковые настройки
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US", false);
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);



            // Меняем приоритет текущего процесса (для общего развития)
            Process ps = Process.GetCurrentProcess();
            ps.PriorityClass = ProcessPriorityClass.Idle;

            //// Это уже меняем приоритет запущенного процесса
            //Process p = new Process();
            //p.StartInfo.FileName = textBox1.Text;
            //p.Start();
            //Process.GetProcessById(p.Id).PriorityClass = ProcessPriorityClass.RealTime;




            Console.Write("Добро пожаловать.");// Введите адрес файла, содержащего константы для расчёта и адрес файла-белка:\n\t");
            string ConstFileName = "dataFile.txt",// Console.ReadLine();
                   CalculationDirectory,
                   ProteinDirectory;

            CalculationDirectory = "Calculation_" + String.Format("{0}.{1}.{2}_{3}-{4}-{5}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            Directory.CreateDirectory(CalculationDirectory);
            StreamWriter logFile = new StreamWriter(CalculationDirectory + @"\LogFileCalculation.txt");

            //for (double testCellsD = 0.36; testCellsD >= 0.34; testCellsD -= 0.001) //тест на сходимость при уменьшении размеров ячейки
            {
                try
                {
                    //Создаём глобальный файл данных по параметрам описанным в файле настроек dataFile.txt
                    DataBank dataBank = new DataBank(ConstFileName);
                    //dataBank.cellsD = testCellsD;
                    if (dataBank != null)
                    {

                        Console.WriteLine("Список белков для расчёта и константы успешно считаны.\n\tСписок файлов для расчётов:");
                        logFile.WriteLine("Список белков для расчёта и константы успешно считаны.\n\tСписок файлов для расчётов:");
                        logFile.WriteLine("\tСписок файлов для расчётов:");
                        foreach (string fprotname in dataBank.filesProteinName)
                        {
                            Console.WriteLine("\t\t" + fprotname);
                            logFile.WriteLine("\t\t" + fprotname);
                        }
                        Console.WriteLine("\tВаан-дер-Ваальсовый радиус растворителя = " + dataBank.rSolvent.ToString() + "A"
                                         + "\n\tПлотность растворителя = " + dataBank.ro0.ToString()
                                         + "\n\tРазмер ячейки = " + dataBank.cellsD.ToString() + "A"
                                         + "\n\tКоффициент сшивки по трём осям = " + dataBank.m.ToString()
                                         + "\n\tРазбиение = " + dataBank.numberFi.ToString() + "x" + dataBank.numberQz.ToString()
                                         + "\n\tКол. точек = " + dataBank.numberQ.ToString());
                        logFile.WriteLine("\tВаан-дер-Ваальсовый радиус растворителя = " + dataBank.rSolvent.ToString() + "A");
                        logFile.WriteLine("\tПлотность растворителя = " + dataBank.ro0.ToString());
                        logFile.WriteLine("\tРазмер ячейки = " + dataBank.cellsD.ToString() + "A");
                        logFile.WriteLine("\n\tКоффициент сшивки по трём осям = " + dataBank.m.ToString());
                        logFile.WriteLine("\tРазбиение = " + dataBank.numberFi.ToString() + "x" + dataBank.numberQz.ToString());
                        logFile.WriteLine("\tКол. точек = " + dataBank.numberQ.ToString());
                        Console.WriteLine("Начинаю обработку файлов. Время начала расчётов: " + (DateTime.Now).ToString());
                        logFile.WriteLine("Начинаю обработку файлов. Время начала расчётов: " + (DateTime.Now).ToString());

                        //Directory.CreateDirectory(CalculationDirectory);
                        foreach (string fprotname in dataBank.filesProteinName)
                        {
                            ProteinDirectory = CalculationDirectory + @"\" + fprotname;
                            //Directory.CreateDirectory(ProteinDirectory);

                            Console.WriteLine("\t Начинаю обработку файла \"" + fprotname + "\" \n\t\tВремя начала: " + (DateTime.Now).ToString());
                            logFile.WriteLine("\t Начинаю обработку файла \"" + fprotname + "\" ");
                            logFile.WriteLine("\t\tВремя начала: " + (DateTime.Now).ToString());

                            dataBank.ModelCounter(fprotname);
                            Console.WriteLine("\t\tВ файле " + fprotname + " обнаружено моделей: " + ((dataBank.countModel == 0) ? 1 : dataBank.countModel).ToString());
                            logFile.WriteLine("\t\tВ файле " + fprotname + " обнаружено моделей: " + ((dataBank.countModel == 0) ? 0 : dataBank.countModel).ToString());

                            for (int currentModel = (dataBank.countModel == 0) ? 0 : 1;
                                 currentModel <= ((dataBank.countModel == 0) ? 0 : dataBank.countModel);
                                 currentModel++)
                            {

                                dataBank.ProteinRead(fprotname, ref dataBank, currentModel);
                                if (dataBank.countModel == 0) currentModel++;
                                Console.WriteLine("\t\tНачинаю обработку модели № \"" + currentModel.ToString() + "\" \n\t\t\tВремя начала: " + (DateTime.Now).ToString());
                                logFile.WriteLine("\t\tНачинаю обработку модели № \"" + currentModel.ToString() + "\" ");
                                logFile.WriteLine("\t\t\tВремя начала: " + (DateTime.Now).ToString());

                                Console.WriteLine("\t\t\tДанные успешно считаны"
                                                   + "\n\t\t\tРазмеры массива разбиения: " + dataBank.aSizeX.ToString() + "x"
                                                                                         + dataBank.aSizeY.ToString() + "x"
                                                                                         + dataBank.aSizeZ.ToString());
                                logFile.WriteLine("\t\t\tДанные успешно считаны");
                                logFile.WriteLine("\t\t\tРазмеры массива разбиения: " + dataBank.aSizeX.ToString() + "x"
                                                                                         + dataBank.aSizeY.ToString() + "x"
                                                                                         + dataBank.aSizeZ.ToString());

                                Protein.CalculationMolecularSurface(ref dataBank);
                                //Console.WriteLine("\t\tПостроение молекулярного и доступного пространств завершено.");

                                //Console.WriteLine("\t\tNe= " + dataBank.SumElectrons.ToString());
                                //Console.WriteLine("\t\tp = {0}", dataBank.SumElectrons / dataBank.molecularVolume);

                                Protein.SummaryElctrons(ref dataBank);
                                Console.WriteLine("\t\t\tКоличество электронов: Ne={0}", dataBank.SumElectrons);
                                logFile.WriteLine("\t\t\tКоличество электронов: Ne={0}", dataBank.SumElectrons);

                                Console.WriteLine("\t\t\tРадиус инерции: R={0} A", Protein.CalcRadiusInertia(ref dataBank));
                                logFile.WriteLine("\t\t\tРадиус инерции: R={0} A", Protein.CalcRadiusInertia(ref dataBank));

                                Console.WriteLine("\t\t\tОбъёмы: \n\t\t\t\tДоступный объём = " + (dataBank.accessVolume).ToString() + " A^3"
                                                  + "\n\t\t\t\tМолекулярный объём = " + (dataBank.molecularVolume).ToString() + " A^3");
                                logFile.WriteLine("\t\t\tОбъёмы:");
                                logFile.WriteLine("\t\t\t\tДоступный объём = " + (dataBank.accessVolume).ToString() + " A^3");
                                logFile.WriteLine("\t\t\t\tМолекулярный объём = " + (dataBank.molecularVolume).ToString() + " A^3");

                                Console.WriteLine("\t\t\tПлощади поверхностей: \n\t\t\t\tПлощадь доступной = " + (dataBank.accessArea).ToString() + " A^2"
                                + "\n\t\t\t\tПлощадь молекулярной = " + (dataBank.molecularArea).ToString() + " A^2");
                                logFile.WriteLine("\t\t\tПлощади поверхностей:");
                                logFile.WriteLine("\t\t\t\tПлощадь доступной = " + (dataBank.accessArea).ToString() + " A^2");
                                logFile.WriteLine("\t\t\t\tПлощадь молекулярной = " + (dataBank.molecularArea).ToString() + " A^2");

                                Console.WriteLine("\t\t\tПодсчёт кубиков: \n\t\t\tНа поверхностях: \n\t\t\t\tНа доступной: {0} \n\t\t\t\tНа молекулярной: {1} \n\t\t\tВ объёмах: \n\t\t\t\tВ доступном: {2} \n\t\t\t\tВ молекулярном: {3}",
                                                    dataBank.numberAccessSurfaceCells, dataBank.numberMolecularSurfaceCells,
                                                    dataBank.NumberAccessVolumeCell, dataBank.NumberMolecularVolumeCell);
                                logFile.WriteLine("\t\t\tПодсчёт кубиков: ");
                                logFile.WriteLine("\t\t\t\tНа поверхностях:");
                                logFile.WriteLine("\t\t\t\t\tНа доступной:  " + dataBank.numberAccessSurfaceCells.ToString());
                                logFile.WriteLine("\t\t\t\t\tНа молекулярной: " + dataBank.numberMolecularSurfaceCells.ToString());
                                logFile.WriteLine("\t\t\t\tВ объёмах: ");
                                logFile.WriteLine("\t\t\t\t\tВ доступном: " + dataBank.NumberAccessVolumeCell.ToString());
                                logFile.WriteLine(" \t\t\t\t\tВ молекулярном: " + dataBank.NumberMolecularVolumeCell.ToString());


                                //Здесь добавлять вызов новых расчётов
                                
                                /*
                                //Вызов окна визуализации белка
                                PrintStructWindows printStructWindows = new PrintStructWindows(ref dataBank);
                                printStructWindows.ShowDialog();
                                printStructWindows.Dispose();
                                */

                                //dataBank.WriteReport(fprotname, ref dataBank);
                                //Console.WriteLine("\t\tПапка с отчётом создана.");


                                Protein.GetChainCubes(ref dataBank);
                                dataBank.aquarium = new Cell[0, 0, 0];
                                

                                logFile.Close();
                                logFile = File.AppendText(CalculationDirectory + @"\LogFileCalculation.txt");

                                //Console.WriteLine("Для начала расчёта интенсивности нажимте enter");
                                //Console.ReadKey();

                                //for (int numbRoO = 3; numbRoO <= 3; numbRoO++)
                                //{
                                //switch (numbRoO)
                                //{
                                //    case 1:
                                //        {
                                //            dataBank.ro0 = 0;
                                //            break;
                                //        }
                                //    case 2:
                                //        {
                                //            dataBank.ro0 = 0.334;
                                //            break;
                                //        }
                                //    case 3:
                                //        {
                                //            dataBank.ro0 = 0.41;
                                //            break;
                                //        }
                                //}

                                //----->Так как нужны только площади
                                dataBank.IofQ = IntensityofQ.CalculationScatteringIntensity(dataBank);
                                Reporter.ReportIntensityTxt(ProteinDirectory, fprotname, ref dataBank, currentModel);
                                //<-----

                                //Console.WriteLine("\t\tВремя завершения: " + (DateTime.Now).ToString() + "\n\tОбработка модели № \"" + currentModel  + "\" для роО=" + dataBank.ro0.ToString() + " завершена");
                                //logFile.WriteLine("\t\tВремя завершения: " + (DateTime.Now).ToString());
                                //logFile.WriteLine("\tОбработка модели № \"" + currentModel  + "\" для роО=" + dataBank.ro0.ToString() + " завершена");
                                //}



                                dataBank.IofQ.Clear();
                                dataBank.listChainCubs.Clear();

                                Console.WriteLine("\t\t\tВремя завершения: " + (DateTime.Now).ToString() + "\n\t\tОбработка модели № \"" + currentModel + " завершена");
                                logFile.WriteLine("\t\t\tВремя завершения: " + (DateTime.Now).ToString());
                                logFile.WriteLine("\t\tОбработка модели № \"" + currentModel + " завершена");

                                logFile.Close();
                                logFile = File.AppendText(CalculationDirectory + @"\LogFileCalculation.txt");
                            }

                            Console.WriteLine("\t\tВремя завершения: " + (DateTime.Now).ToString() + "\n\tОбработка файла \"" + fprotname + "\" завершена");
                            logFile.WriteLine("\t\tВремя завершения: " + (DateTime.Now).ToString());
                            logFile.WriteLine("\tОбработка файла \"" + fprotname + "\" завершена");
                        }
                        Console.WriteLine("Обработка файлов завершена. Время завершения расчётов: " + (DateTime.Now).ToString()
                                          + "\n\nДля выхода подвердите ввод.");
                        logFile.WriteLine("Обработка файлов завершена. Время завершения расчётов: " + (DateTime.Now).ToString());



                    }



                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    logFile.WriteLine(e.Message);
                }
            }

            logFile.Close();
            Console.ReadKey();
        }


    }

}
